import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable()

export class DataService {
  urlApi = 'http://localhost:8001/steps';

  constructor(private http: HttpClient) {}

  getData() {
    return this.http.get(this.urlApi);
  }
}

