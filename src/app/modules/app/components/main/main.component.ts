// Models
import {Step} from '../../models/step';

// Components
import { Component, OnInit, Input } from '@angular/core';

// Services
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass', '../../../../app.component.sass'],
  providers: [DataService]
})
export class MainComponent implements OnInit {
  items: Step[] = [];
  stepList: Step[];
  price_per_hour = 0;
  total_price = 0;
  os_option_id: number;
  screen_number_id: number;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getData().subscribe((data: Step[]) => this.stepList = data);
  }

  option_click(step_index, is_step_root, option_id, optionValues, stepValues) {
    if (is_step_root) {
      this.os_option_id = option_id;

      for (let i = 0; i < optionValues.length; i++) {
        if (optionValues[i].relatedId === this.os_option_id) {
          this.price_per_hour = optionValues[i].minValue;
          this.total_price = this.price_per_hour;
          this.total_price -= this.total_price % 1;
        }
      }
    }
    if (step_index === 1) {
      for (let i = 0; i < stepValues.length; i++) {
        if (stepValues[i].id === option_id) {
          this.screen_number_id = i;
        }
      }
    }
    if (!is_step_root && !(step_index === 1)) {
      for (let i = 0; i < optionValues.length; i++) {
          if (optionValues[i].relatedId === this.os_option_id) {
            this.total_price += this.price_per_hour * ((optionValues[i].maxValue - optionValues[i].minValue) /
              (optionValues.length - 1) * this.screen_number_id + optionValues[i].minValue);
            this.total_price -= this.total_price % 1;
          }
      }
    }
  }
}
