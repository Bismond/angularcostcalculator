import {Option} from './option';

export class Step {
  constructor(public id: number,
              public name: string,
              public orderNumber: number,
              public isStepRoot: boolean,
              public stepOptions: Option[]) {}
}

