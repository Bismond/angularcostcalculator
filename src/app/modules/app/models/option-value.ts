export class OptionValue {
  constructor(  public id: number,
                public optionId: number,
                public relatedId: number,
                public minValue: number,
                public maxValue: number) {}

}
