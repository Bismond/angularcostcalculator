import {OptionValue} from './option-value';

export class Option {
  constructor(public id: number,
              public name: string,
              public stepId: number,
              public optionValues: OptionValue[]) {}
}
